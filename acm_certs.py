import boto3
import logging
import sys
from pprint import pprint
import botocore

logging.basicConfig(stream=sys.stdout, level=logging.ERROR)

regions = boto3.client("ec2", region_name="us-east-2").describe_regions()

email_validated_certs = {}

for region in regions["Regions"]:
    region_name = region["RegionName"]
    logging.info(f"Region name: {region_name}")

    client = boto3.client("acm", region_name=region_name)

    try:
        response = client.list_certificates(CertificateStatuses=["ISSUED"])

        for certificate in response["CertificateSummaryList"]:
            cert_response = client.describe_certificate(
                CertificateArn=certificate["CertificateArn"]
            )
            logging.info(cert_response)

            # Check cert for EMAIL validation method and is being used.
            if (
                cert_response["Certificate"]["DomainValidationOptions"][0][
                    "ValidationMethod"
                ]
                == "EMAIL"
            ) and cert_response["Certificate"]["InUseBy"]:
                certificate = cert_response["Certificate"]
                domain = certificate["DomainName"]
                san = certificate["SubjectAlternativeNames"]
                in_use_by = certificate["InUseBy"]
                arn = certificate["CertificateArn"]
                print("---")
                print(
                    f"Update certificate for {domain} in {region_name} in the production account"
                )
                print("---")
                print(
                    f"""As a Cloud Engineer, I want to stop validating our ACM certs via email and instead validate them via DNS so that they automatically renew.  An email validated cert can not be directly converted to a DNS validated cert so new certs must be requested.  This ticket is to issue a new certificate for {domain} in {region_name} and associate it with all of the resources the current certificate is bound to.
                
h1. Acceptance Criteria
* Issue a new certificate for {domain} (Don't forget SANs!)
* Update AWS resources to use the new certificate.
* Validate the old certificate is no longer bound to any resources (see helper AWS CLI command below)

h1. Notes
The existing resources bound to this certificate are below.  IMPORTANT This list could have changed since the ticket was submitted.  You can check the bound resources via the AWS CLI with:
{{code}}AWS_PROFILE=contrast-production aws acm describe-certificate --certificate-arn {arn} --region {region_name}{{code}}

{in_use_by}


                """
                )
                print("---")

                input("Press Enter for the next cert...")
                # relevant_keys = [
                #     "CertificateArn",
                #     "DomainName",
                #     "InUseBy",
                #     "Subject",
                #     "SubjectAlternativeNames",
                #     "Type",
                # ]
                # cert = {
                #     relevant_key: certificate[relevant_key]
                #     for relevant_key in relevant_keys
                # }
                # if not email_validated_certs.get(region_name):
                #     email_validated_certs[region_name] = []
                # email_validated_certs[region_name].append(cert)
    except botocore.exceptions.ClientError as err:
        logging.error(f"Error with region {region_name}: {err}")


# for region_name, certificates in email_validated_certs.items():
#     for certificate in certificates[0]:
#         domain = certificate["DomainName"]
#         san = certificate["SubjectAlternativeNames"]
#         in_use_by = certificate["InUseBy"]
#         arn = certificate["CertificateArn"]

#         pprint(f"Update certificate for {domain} in {region_name})")
