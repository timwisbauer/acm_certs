# acm_certs

# Description

Generates ticket description and summary for replacing certificates that use email validation.

# Prerequsites

* Python 3 (Written with 3.9, other versions untested)
* AWS profile configured

# Example

```AWS_PROFILE=contrast-staging python3 acm_certs.py```
